package person.favorites

import person.types.PersonWithAddress
import io.reactivex.Observable

internal class FavoritesRepository(private val personBackend: PersonBackend, private val favoritesDatabase: FavoritesDatabase) {


    fun loadFavorites(): Observable<List<PersonWithAddress>> {
        val contactsObservable = favoritesDatabase.favoriteContacts()
        val personObservable = personBackend.loadAllPersons()

        return contactsObservable.flatMap { set ->
            personObservable.map { listOfPersons ->
                listOfPersons.filter { set.contains(it.person.id) }
            }
        }
    }
}
