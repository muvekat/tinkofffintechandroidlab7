package fibonacci;

import io.reactivex.Observable;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class FibonacciExercises {

	public Observable<Integer> fibonacci(int n) {
		return Observable.create(source -> {
			int n1 = 0;
			int n2 = 1;
			int tmp;

			for(int i = 0; i < n; i++){
				source.onNext(n1);

				tmp = n1 + n2;
				n1 = n2;
				n2 = tmp;
			}

			source.onComplete();
		});
	}
}
